<?php
    // Esta es el "dispatcher" de usuarios

    // En cada script hay que cargar el init.php
    include_once $_SERVER['DOCUMENT_ROOT']."/../app/init.php";

    // Se cargan las clases que se ocupan
    include_once CONTROLLERS."/ColegiosController.php";
    include_once MODELS."/Colegio.php";

    // Se hace el "use" de las clases que se utilizaran en este script
    use MyApp\Controllers\ColegiosController;
    $controller = new ColegiosController($config, $login);

    // Si viene asignado el parámetro "show" se muestra la vista "show"
    if (isset($_GET['show']))
        $controller->show($_GET['show']);

    // Si viene asignado el parámetro "edit" se muestra la vista "edit"
    elseif (isset($_GET['edit']))
        $controller->edit($_GET['edit']);

    // Si viene asignado el parámetro "delete" se llama al método "destroy"
    elseif (isset($_GET['delete']))
        $controller->destroy($_GET['delete']);

    // Otras acciones se capturan por medio del parámetro "action"
    elseif (isset($_GET['action'])){
        switch ($_GET['action']) {
            case 'new':
                $controller->create();
                break;
            case 'save':
                $controller->store($_POST);
                break;
            case 'update':
                $controller->update($_POST);
                break;
            default:
                break;
        }
    }

    // En cualquier otro caso se muestra la vista "index"
    else
        $controller->index();