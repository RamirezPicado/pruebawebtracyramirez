<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;

class Colegio
{
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAll($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM colegios_sancarlos where id=".$id);

      $this->db->disconnect();

      return $this->db->getAll($result);
    }
    /**
     * Obtiene un registro de usuario
     */
    public function getById($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM colegios_sancarlos WHERE id=".$id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de usuario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insert($data)
    {

      // https://www.php.net/manual/es/mysqli.quickstart.prepared-statements.php
      // https://www.php.net/manual/es/mysqli-stmt.bind-param.php

      $this->db->connect();
      $sql = "INSERT INTO colegios_sancarlos (lugar_colegio,nombre_colegio,direccion_colegio,
      fecha_fundacion,tipo_colegio) VALUES (?,?,?,?,?)";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("sssss",$data['lugar_colegio'], $data['nombre_colegio'],$data['direccion_colegio'],
        $data['fecha_fundacion'],$data['tipo_colegio']);
        $stmt->execute();
        $stmt->close();

      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Actualiza un registro de "user" en la base de datos
    * @param user contiene los datos del registro de usuario a actualizar
    */
    public function update($data)
    {
      $this->db->connect();

        $sql = "UPDATE questions SET question_text = ? WHERE id = ?";

      $stmt = $this->db->prepareSQL($sql);
      if($stmt) {

        $stmt->bind_param("ss", $data['question_text'], $data['id']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

    /**
    * Elimina un registro de la base de datos
    * @param id del registro que se desea eliminar
    */
    public function delete($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM questions WHERE id = ?";
      $sql2 = "DELETE FROM answers WHERE question_id = ?";

      if($stmt = $this->db->prepareSQL($sql2)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }



      $this->db->disconnect();
    }

}
}
