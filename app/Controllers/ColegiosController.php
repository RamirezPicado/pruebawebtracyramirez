<?php namespace MyApp\Controllers {

    use MyApp\Models\User;
    use MyApp\Utils\Message;

    class ColegiosController
    {
        private $config = null;
        private $login = null;
        private $colegioModel = null;

        /**
         * Constructor de controlador de usuarios
         * 
         * @param config contiene la configuración para la conexión de BD
         * @param login contiene los datos del usuario que inició sesión
         */
        public function __construct($config, $login)
        {
            $this->config = $config;
            $this->login = $login;
        }

        /**
         * Muestra la vista "Index" del catálogo de usuarios
         */
        public function index(){
            $colegioModel = new Colegio($this->config);
            $collection = $colegioModel->getAll();

            $dataToView = ["collection" => $collection,
                           "login" => $this->login ];

            return view("colegios/index.php", $dataToView);
        }

        /**
         * Muestra el formulario para crear un nuevo registro
         */
        public function create()
        {
            $dataToView = [ "login" => $this->login ];
            return view("colegios/new.php", $dataToView);
        }

        /**
         * Almacena un registro nuevo de "user" en la base de datos
         * @param request contiene los datos del nuevo registro de usuario
         */
        public function store($request)
        {
            $colegioModel = new Colegio($this->config);
            $colegioModel->insert($request);
            header('Location: /colegios');
        }

        /**
         * Muestra la vista "show" con los detalles del registro "id"
         * @param id del registro del cual se deben mostrar los detalles
         */
        public function show($id){
            $colegioModel = new Colegio($this->config);
            $colegio = $colegioModel->get($id);
            $dataToView = ["login" => $this->login,
                           "colegios" => $colegio ];
            return view("colegios/show.php", $dataToView);
        }

        /**
        * Muestra para vista "edit" con los detalles del registro "id"
        * @param id del registro del cual se deben cargar los detalles para edición
        */
        public function edit($id){
            $colegioModel = new Colegio($this->config);
            $colegio= $colegioModel->get($id);
            $dataToView = ["login" => $this->login,
                           "colegios" => $colegio ];
            return view("colegios/edit.php", $dataToView);
        }

        /**
        * Actualiza un registro de "user" en la base de datos
        * @param request contiene los datos del registro de usuario a actualizar
        */
        public function update($request)
        {
            $colegioModel = new Colegio($this->config);
            $colegioModel->update($request);
            //$this->index();
            header('Location: /colegios');
        }

        /**
         * Elimina un registro de la base de datos
         * @param id del registro que se desea eliminar
         */
        public function destroy($id)
        {
            $colegioModel = new Colegio($this->config);
            $colegioModel->delete($id);
            header('Location: /colegios');
        }
    }

}