<?php 
  include VIEWS.'/partials/header.php';
  include VIEWS.'/partials/navbar.php';
?>
  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h1>Colegios</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="text-center">Ver</th>
              <th class="text-center">Editar</th>
              <th class="text-center">Eliminar</th>
              <th scope="col">id</th>
              <th scope="col">lugar_colegio</th>
              <th scope="col">nombre_colegio</th>
              <th scope="col">direccion_colegio</th>
              <th scope="col">fecha_fundacion</th>
              <th scope="col">tipo_colegio</th>
              
            </tr>
          </thead>
          <tbody>
            <?php foreach ($collection as $item): ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/colegios/index.php?show=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/colegios/index.php?edit=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/colegios/index.php?delete=".$item['id']; ?>"></a>
              </td>
              <td><?= $item['id']; ?></td>
              <td><?= $item['lugar_colegio']; ?></td>
              <td><?= $item['nombre_colegio']; ?></td>
              <td><?= $item['direccion_colegio']; ?></td>
              <td><?= $item['fecha_fundacion']; ?></td>
              <td><?= $item['tipo_colegio']; ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <a class="btn btn-primary" href="/colegios/index.php?action=new">Agregar Colegio</a>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
