<?php include VIEWS.'/partials/header.php';
      include VIEWS.'/partials/navbar.php'; ?>
  <div class="container">
    <br>
    <?php include VIEWS.'/partials/message.php' ?>
    <div class="row">
      <div class="col-sm-6">
        <h1>Agregar Colegio</h1>
        <form action="/colegios/index.php?action=save" method="post">
          <div class="form-group">
            <label for="lugar_colegio">Lugar de Colegio</label>
            <input 
              type="text" class="form-control" id="lugar_colegio" name="lugar_colegio">
          </div>
          <div class="form-group">
            <label for="nombre_colegio">Nombre de Colegio</label>
            <input 
              type="text" class="form-control" id="nombre_colegio" name="nombre_colegio">
          </div>
          <div class="form-group">
            <label for="direccion_colegio">Direccion de Colegio</label>
            <input 
              type="text" class="form-control" id="direccion_colegio" name="direccion_colegio">
          </div>
          <div class="form-group">
            <label for="fecha_fundacion">Fecha de Fundacion</label>
            <input 
              type="text" class="form-control" id="direccion_colegio" name="direccion_colegio">
          </div>
          
          <div class="form-group">
            <label for="tipo_colegio">Tipo de Colegio</label>
            <select class="form-control"id="tipo_colegio" name="tipo_colegio">
              <option value="liceo">Liceo</option>
              <option value="tecnico" id="tipo_colegio" name="tipo_colegio">Tencico</option>
            </select>
            </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/colegios/index.php">Regresar</a>
        </form>
        
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>