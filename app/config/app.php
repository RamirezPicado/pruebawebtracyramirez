<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Parámetros generales
    |--------------------------------------------------------------------------
    | Definir en los parámetros generales de la aplicación
    */
    'debug'      => true,
    'public'     => "/",

    /*
    |--------------------------------------------------------------------------
    | Database parameters
    |--------------------------------------------------------------------------
    | En esta sección se deben definir los parámetros de conexión a BD
    */
    'server'       => 'localhost',
    'database'     => 'colegios',
    'user'         => 'colegios',
    'password'     => 'secret',
];
