<?php

namespace EasilyPHP\View {

    class ViewFactory {

        private $view = null;
        private $data = null;

        /**
         * Crea una nueva instancia del renderizador de vistas
         *
         * @param $view
         * @param $data
         */
        public function __construct($view, $data = [])
        {
            $this->view = VIEWS.'/'.$view;
            $this->data = $data;
        }

        /**
         * Renderiza la vista y extrae los parámetros
         */
        public function render()
        {
            extract($this->data);
            include_once $this->view;
        }
    }
}